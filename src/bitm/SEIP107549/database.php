<?php
namespace src\bitm\SEIP107549;
class database  {
	public static $link = "";
	public static $db = null;
 
	public static function connect($host="localhost", $user="root", $pass="", $dbname="project") {
		self::$db = new PDO("mysql:host=".$host.";dbname=".$dbname.";charset=utf8", $user, $pass);
	}

	public static function destroy() {
		database::$db = null;
	}

	public static function dump($data = false) {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	}
}

?>