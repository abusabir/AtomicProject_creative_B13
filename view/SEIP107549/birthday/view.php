<?php $module_for="birthday"; ?>
<div class="second_menue">
	<a type="button" class="btn btn-default" href="?view=<?php echo $module_for; ?>&action=view">View List</a>
	<a type="button" class="btn btn-primary" href="?view=<?php echo $module_for; ?>&action=create" >Create</a>

</div>

<?php

	use src\bitm\SEIP107549\birthday\birthday;
	$acs_birthday = new birthday();
	$birthdays = $acs_birthday->index();
	
	if (isset($_POST['btnDelete'])) {
		$acs_birthday->delete($_POST['column_id']);
	}
?>

	 <table class="table">
	    <thead>
	      <tr>
	      	<th>Serial No.</th>
	        <th>Book Name</th>
	        <th>Authar Name</th>
	        
	      </tr>
	    </thead>
	    <tbody>
	    	<?php
            $slno =1;
            foreach($birthdays as $acs_birthday){
            ?>
	      <tr class="success">
            <td><?php echo $acs_birthday->id;?></td>
             <td><a href="#"><?php echo $acs_birthday->name;?></a></td>
            <td><?php echo $acs_birthday->birthday;?></td>
	        <td>
	        	<form action="?view=birthday&action=edit" method="post" style="float:left;">
					<input type="submit" class="btn btn-primary" value="Edit">
					<input type="hidden" name="column_id" value="<?php echo $acs_birthday->id; ?>">
				</form>
				<form action="?view=birthday&action=view" method="post">
					<input type="hidden" name="column_id" value="<?php echo $acs_birthday->id; ?>">
					<input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
	        </td>

	      </tr>
	     	<?php
            }
            ?>
	    </tbody>
	  </table>