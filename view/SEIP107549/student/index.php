<?php
use src\bitm\SEIP107549\student\Student;
$class = new Student();
if (isset($_POST['btnDelete'])) {
	$class->delete($_POST['column_id']);
}

$list = $class->index();
if (is_array($list) && !empty($list)) {
	?>
	<table class="table">
	<thead>
		<tr>
			<td>Serial</td>
			<td>Name</td>
			<td>Father's Name</td>
			<td>Gender</td>
			<td>Education</td>
			<td>Birthday</td>
			<td>About</td>
			<td>Email</td>
			<td>City</td>
			<td>Interest</td>
			<td>Action</td>
		</tr>
	</thead>
	<?php
	$serial = 0;
	foreach ($list as $key => $row) {
		$serial++;
		?>
		<tr>
			<td><?php echo $serial; ?></td>
			<?php
			$photo = glob("photo/".$row['id']."*");
			?>
			<td><img src="<?php echo $photo[0]; ?>" style="width:120px;"></td>
			<?php
			foreach ($row as $key => $column) {
				if ($key != "id") {
					if ($key == "date_of_birth") {
						?>
						<td><?php echo date("d-m-Y", strtotime($column)); ?></td>
						<?php
					} elseif ($key == "education" || $key == "interest") {
						?>
						<td><?php echo implode(", ", unserialize($column)); ?></td>
						<?php
					} else {
						?><td><?php echo $column; ?></td><?php
					}
				}
			}
			?>
			<td>
				<form action="?view=<?php echo $_GET['view']; ?>&action=edit" method="post" class="form-inline" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-warning" name="btnEdit" value="Edit">
				</form>
				<form class="form-inline" action="" method="post" style="float:left; margin-right:10px;">
					<input type="hidden" name="column_id" value="<?php echo $row['id']; ?>"><input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
			</td>
			<?php
			?>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
}