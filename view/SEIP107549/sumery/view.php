<?php $module_for="sumery"; ?>
<div class="second_menue">
	<a type="button" class="btn btn-default" href="?view=<?php echo $module_for; ?>&action=view">View List</a>
	<a type="button" class="btn btn-primary" href="?view=<?php echo $module_for; ?>&action=create" >Create</a>

</div>

<?php

	use src\bitm\SEIP107549\sumery\sumery;
	$acs_sumery = new sumery();
	$sumerys = $acs_sumery->index();
	
	if (isset($_POST['btnDelete'])) {
		$acs_sumery->delete($_POST['column_id']);
	}
?>

	 <table class="table">
	    <thead>
	      <tr>
	      	<th>Serial No.</th>
	        <th>Subject</th>
	        <th>Sumery</th>
	        
	      </tr>
	    </thead>
	    <tbody>
	    	<?php
            $slno =1;
            foreach($sumerys as $acs_sumery){
            ?>
	      <tr class="success">
            <td><?php echo $acs_sumery->id;?></td>
             <td><a href="#"><?php echo $acs_sumery->sub;?></a></td>
            <td><?php echo $acs_sumery->sumery;?></td>
	        <td>
	        	<form action="?view=sumery&action=edit" method="post" style="float:left;">
					<input type="submit" class="btn btn-primary" value="Edit">
					<input type="hidden" name="column_id" value="<?php echo $acs_sumery->id; ?>">
				</form>
				<form action="?view=sumery&action=view" method="post">
					<input type="hidden" name="column_id" value="<?php echo $acs_sumery->id; ?>">
					<input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
				
	        </td>

	      </tr>
	     	<?php
            }
            ?>
	    </tbody>
	  </table>