<?php $module_for="picture"; ?>
<div class="second_menue">
	<a type="button" class="btn btn-default" href="?view=<?php echo $module_for; ?>&action=view">View List</a>
	<a type="button" class="btn btn-primary" href="?view=<?php echo $module_for; ?>&action=create" >Create</a>
</div>
<?php
use src\bitm\SEIP107549\picture\picture;
$acs_picture = new picture();
	if(isset($_POST['btnDelete'])){
		$acs_picture->delete($_POST['column_id']);
	}

	$pictures = $acs_picture->index();
	$destination = $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."project".DIRECTORY_SEPARATOR."BITM".DIRECTORY_SEPARATOR."xamprep".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR;
?>

	<table class="table">
	    <thead>
	      <tr>
	      	<th>Serial No.</th>
	        <th>Name</th>
	        <th>Picture</th>
	        
	      </tr>
	    </thead>
	    <tbody>
	    	<?php
            $slno =1;
            foreach($pictures as $acs_picture){
            ?>
	      <tr class="success">
            <td><?php echo $acs_picture->id; ?></td>
             <td><a href="#"><?php echo $acs_picture->name;?></a></td>
            <td><img src="images/<?php echo $acs_picture->picture; ?>" alt="No Image" style="width:80px; height:100px"> </td>
	        <td>
	        	<form action="?view=picture&action=edit" method="post" style="float:left;">
					<input type="submit" class="btn btn-primary" value="Edit">
					<input type="hidden" name="column_id" value="<?php echo $acs_picture->id; ?>">
				</form>
				<form action="?view=picture&action=view" method="post">
					<input type="hidden" name="column_id" value="<?php echo $acs_picture->id; ?>">
					<input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
	        </td>

	      </tr>
	     	<?php
            }

            ?>
	    </tbody>
	  </table>