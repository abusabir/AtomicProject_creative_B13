<?php $module_for="book"; ?>
<div class="second_menue">
	<a type="button" class="btn btn-default" href="?view=<?php echo $module_for; ?>&action=view">View List</a>
	<a type="button" class="btn btn-primary" href="?view=<?php echo $module_for; ?>&action=create" >Create</a>

</div>

<?php

	use src\bitm\SEIP107549\book\book;
	$acs_book = new book();

	if (isset($_POST['btnDelete'])) {
		$acs_book->delete($_POST['column_id']);
	}
	$books = $acs_book->index();
?>

	 <table class="table">
	    <thead>
	      <tr>
	      	<th>Serial No.</th>
	        <th>Book Name</th>
	        <th>Authar Name</th>
	        
	      </tr>
	    </thead>
	    <tbody>
	    	<?php
            $slno =1;
            foreach($books as $acs_book){
            ?>
	      <tr class="success">
            <td><?php echo $acs_book->id;?></td>
             <td><a href="#"><?php echo $acs_book->title;?></a></td>
            <td><?php echo $acs_book->authar;?></td>
	        <td>
	        	<form action="?view=book&action=edit" method="post" style="float:left;">
					<input type="submit" class="btn btn-primary" value="Edit">
					<input type="hidden" name="column_id" value="<?php echo $acs_book->id; ?>">
				</form>
				<form action="?view=book&action=view" method="post">
					<input type="hidden" name="column_id" value="<?php echo $acs_book->id; ?>">
					<input type="submit" class="btn btn-danger" name="btnDelete" value="Delete">
				</form>
	        </td>

	      </tr>
	     	<?php
            }
            ?>
	    </tbody>
	  </table>